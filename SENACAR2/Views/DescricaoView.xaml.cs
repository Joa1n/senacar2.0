﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SENACAR2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DescricaoView : ContentPage
    {
        private const float VIDROELETRICO = 80;
        private const float TRAVAELETRICA = 40;
        private const float ARCONDICIONADO = 30;
        private const float CAMERARE = 25;

        public string TextoVidroEletrico
        {
            get
            {
                return string
                    .Format("Transporte - R$ {0}", VIDROELETRICO);
            }
        }

        public string TextoTravaEletrica
        {
            get
            {
                return string
                    .Format("Banho Rápido - R$ {0}", TRAVAELETRICA);
            }
        }
        public string TextoArCondicionado
        {
            get
            {
                return string
                    .Format("Passeio - R$ {0}", ARCONDICIONADO);
            }
        }

        public string TextoCameraRe
        {
            get
            {
                return string
                    .Format("Massagem - R$ {0}", CAMERARE);
            }
        }

        public string ValorTotal
        {
            get
            {
                return string.Format("Valor total: R$ {0}",
                    Carros.Preco
                    + (IncluiVidroEletrico ? VIDROELETRICO : 0)
                    + (IncluiTravaEletrica ? TRAVAELETRICA : 0)
                    + (IncluiArCondicionado ? ARCONDICIONADO : 0)
                    + (IncluiCameraRe ? CAMERARE : 0)
                    );
            }
        }

        // Capturando opcionais --

        bool incluiVidroEletrico;
        public bool IncluiVidroEletrico
        {
            get
            {
                return incluiVidroEletrico;
            }

            set
            {
                incluiVidroEletrico = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiTransporte)
                //    DisplayAlert("Transporte", "Ativo", "OK");
                //else
                //    DisplayAlert("Transporte", "Inativo", "OK");
            }
        }

        bool incluiTravaEletrica;
        public bool IncluiTravaEletrica
        {
            get
            {
                return incluiTravaEletrica;
            }

            set
            {
                incluiTravaEletrica = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiBanhoRapido)
                //    DisplayAlert("Transporte", "Ativo", "OK");
                //else
                //    DisplayAlert("Transporte", "Inativo", "OK");
            }
        }

        bool incluiArCondicionado;
        public bool IncluiArCondicionado
        {
            get
            {
                return incluiArCondicionado;
            }

            set
            {
                incluiArCondicionado = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiPasseio)
                //    DisplayAlert("Transporte", "Ativo", "OK");
                //else
                //    DisplayAlert("Transporte", "Inativo", "OK");
            }
        }

        bool incluiCameraRe;
        public bool IncluiCameraRe
        {
            get
            {
                return incluiCameraRe;
            }

            set
            {
                incluiCameraRe = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiMassagem)
                //    DisplayAlert("Transporte", "Ativo", "OK");
                //else
                //    DisplayAlert("Transporte", "Inativo", "OK");
            }
        }

        public Carros Carros { get; set; }
        public DescricaoView(Carros carros)
        {
            InitializeComponent();

            this.Title = carros.Nome;
            this.Carros = carros;
            this.BindingContext = this;

        }

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {

            Navigation.PushAsync(new AgendamentoView(this.Carros));
        }
    }
}