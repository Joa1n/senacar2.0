﻿using SENACAR2.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SENACAR2
{
    public class Carros
    {
        public string Nome { get; set; }
        public float Preco { get; set; }
        public string PrecoFormatado
        {
            get { return string.Format("R$ {0}", Preco); }
        }
    }
    public partial class ListagemView : ContentPage
    {
        public List<Carros> Carros { get; set; }



        public ListagemView()
        {
            InitializeComponent();


            this.Carros = new List<Carros>
            {
                new Carros {Nome= "Chevrolet Camaro", Preco = 50010},
                new Carros {Nome= "Hyundai HB20", Preco = 42000},
                new Carros {Nome= "Renault Senic", Preco = 39000},
                new Carros {Nome= "Ferrari", Preco  = 84054100},
            };

            this.BindingContext = this;
        }

        private void InitializeComponent()
        {
            throw new NotImplementedException();
        }

        private void ListViewsCarros_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var carros = (Carros)e.Item;

            Navigation.PushAsync(new DescricaoView(carros));
        }
    }
}
